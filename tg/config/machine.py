from taurus.qt.qtgui.taurusgui.utils import PanelDescription


trend = PanelDescription(
    "Trend",
    module="taurus_pyqtgraph.trend",
    classname="TaurusTrend",
    floating=False,
    model=[
        "eval:3",
        "eval:5",
        "eval:7",
        "eval:9",
    ]

)
