import sys

from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.taurusgui import TaurusGui

try:
    from pathlib import Path
except ImportError:
    from pathlib2 import Path


class TG(TaurusGui):
    def __init__(self, parent=None, confname=None):
        TaurusGui.__init__(self, parent=parent, confname=confname)

        self.jorgsBar.hide()

        conf = Path(__file__).resolve().parent / "config"
        print(str(conf))
        print()
        self.loadConfiguration(str(conf))


def run_tg():
    app = TaurusApplication(sys.argv)

    m = TG()
    m.show()

    sys.exit(app.exec_())


if __name__ == "__main__":
    run_tg()
